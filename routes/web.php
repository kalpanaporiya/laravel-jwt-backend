<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/web/retrieve-list-of-records', [DataController::class, 'retrieve_list_of_records']);
Route::get('/web/add-new-records', [DataController::class, 'add_new_records']);
Route::get('/web/generate_new_token', [DataController::class, 'generate_new_token']);
