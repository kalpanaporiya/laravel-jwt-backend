<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller {

    /**
     * @name open
     * @param Request $request
     * @return json array
     * @author KPO
     */
    public function open() {
        $data = "This data is open and can be accessed without the client being authenticated";
        return response()->json(compact('data'), 200);
    }

    /**
     * @name closed
     * @param Request $request
     * @return json array
     * @author KPO
     */
    public function closed() {
        $data = "Only authorized users can see this";
        return response()->json(compact('data'), 200);
    }

    /**
     * @name retrieve_list_of_records
     * @param Request $request
     * @return json array
     * @author KPO
     */
    public function retrieve_list_of_records() {
        try {

            $curl = curl_init();
            $url = 'https://app.iformbuilder.com/exzact/api/v60/profiles/502723/pages/3836629/records?fields=emp_name,job_name,dept_id,hire_date,salary,emp_photo&limit=100&offset=0&subform_order=desc';
//            $token = "5b76fbab7f032f2e4903085e30e2f01a85c68a65";
            $token = '';
            if ($this->generate_new_token() != false) {
                $token = $this->generate_new_token();
            } else {
                return response()->json(compact('token invalid'), 500);
            }
            curl_setopt_array($curl,
                    array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_SSL_VERIFYPEER => FALSE,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => TRUE,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array('Authorization: Bearer ' . $token)
                    )
            );

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            return response()->json(compact('response'), 200);
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    /**
     * @name add_new_records
     * @param Request $request
     * @return json array
     * @author KPO
     */
    public function add_new_records(Request $request) {
        try {
            $curl = curl_init();
            $url = 'https://app.iformbuilder.com/exzact/api/v60/profiles/self/pages/3836629/records';
//            $token = "5b76fbab7f032f2e4903085e30e2f01a85c68a65";
            $token = '';
            if ($this->generate_new_token() != false) {
                $token = $this->generate_new_token();
            } else {
                return response()->json(compact('token invalid'), 500);
            }
            curl_setopt_array($curl,
                    array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_SSL_VERIFYPEER => FALSE,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => TRUE,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_HTTPHEADER => array('Authorization: Bearer ' . $token),
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => json_encode($request->all()),
                    )
            );

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            return response()->json(compact('response'), 200);
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    public function generate_new_token() {

        $curl = curl_init();
        $url = 'https://app.iformbuilder.com/exzact/api/oauth/token';

        $iat = time();
        $exp = time() + 300;

        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $base64UrlHeader = base64_encode($header);

        // Create token payload as a JSON string
        $payload_arr = [
            "iss" => "2092c882cff14a29e2beb14d9b954cec3ed31912",
            "aud" => "https://app.iformbuilder.com/exzact/api/oauth/token",
            "exp" => $exp,
            "iat" => $iat
        ];
        $payload = json_encode($payload_arr);
        $base64UrlPayload = base64_encode($payload);

        $secret = 'd7b43990585f703d1c86f094062d3bea8a10c3b3';

        $sig = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($sig));

        $assertion = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_SSL_VERIFYPEER => FALSE,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/x-www-form-urlencoded',
                        'Cookie: PHPSESSID=6so4jr4slq0t87cck4uo74evd1; X-Mapping-fjhppofk=DA61AAF658741B5A573C938E93162983'
                    ),
                    CURLOPT_POST => true,
//                        CURLOPT_POSTFIELDS => 'assertion=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIyMDkyYzg4MmNmZjE0YTI5ZTJiZWIxNGQ5Yjk1NGNlYzNlZDMxOTEyIiwiYXVkIjoiaHR0cHM6Ly9hcHAuaWZvcm1idWlsZGVyLmNvbS9leHphY3QvYXBpL29hdXRoL3Rva2VuIiwiZXhwIjoxNjI0MDg1MTU0LCJpYXQiOjE2MjQwODUxNTR9.xK6mNxoT8-6vRwG6JfTz40fhB7bkNEYloSsZ9NfUkNM&grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer',
                    CURLOPT_POSTFIELDS => 'assertion=' . $assertion . '&grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer',
                )
        );

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        if (!empty($response)) {
            if (isset($response->access_token)) {
                return $response->access_token;
            }
        }
        return false;
    }

}
